module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    express: {
      dev: {
        options: {
          script: 'server.js'
        }
      }
    },
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },
      build: {
        src: 'libs/js/objects.js',
        dest: 'build/libs/js/scripts.min.js'
      }
    },
    sass: {
      dist: {
        options: {
            //style: 'compressed'
        },
        files: [ 
          {'public/css/ui.css': 'public/css/scss/ui.scss'},
          {'public/css/bm_in1.css': 'public/css/scss/bm_in1.scss'},
          {'public/css/bm_karta_tovara.css': 'public/css/scss/bm_karta_tovara.scss'},
          {'public/css/bm_korzina.css': 'public/css/scss/bm_korzina.scss'},
          {'public/css/bm_tovar.css': 'public/css/scss/bm_tovar.scss'},
          {'public/css/index.css': 'public/css/scss/index.scss'}
        ]
      }
    },
    watch: {
      css: {
        files: [
          'public/css/scss/*.scss',
          'public/libs/css/scss/blocks/*.scss',
          'public/libs/css/scss/elements/*.scss',
          'public/libs/css/scss/*.scss'
        ],
        tasks: ['sass'],
        options: {
          spawn: false,
        }
      }
    },
    concat: {
      dest: {
        src: 'public/libs/css/*.css',
        dest: 'public/css/global.css'
      }
    }
  });

  // Load plugins
  grunt.loadNpmTasks('grunt-express-server');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Default task(s).
  grunt.registerTask('default', ['express', 'sass', 'watch']);

};