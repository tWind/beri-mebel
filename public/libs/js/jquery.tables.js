(function($) { 
	$.fn.removeRow = function(options) {
		var settings = {};
		var pane = this;
		var el = $(pane).find('tr');
		var closeButton;

		init(el);
		
		function init(el) {
			settings = $.extend( {
				closePane: '.close-pane'
			}, options);

			for(var i=1; i < el.length; i++) {
				$(pane).find(settings.closePane).append('<div class="button delete">x</div>');
			}
			closeButton = $(pane).find(settings.closePane).children();

			start();
		}

		function start() {
			$(closeButton).each(function(index, elem) {
				$(this).on('click', function() {
					$(el)[index+1].remove();
					$(this).remove();
					console.log(index);
				});
			});
		}
		
	}
})(jQuery);





