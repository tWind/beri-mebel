(function($) { 
	var defaults = {
		viewportClass: 'tw-viewport',
		wrapperClass: 'tw-wrapper',
		mode: 'vertical', // 'horizontal, vertical
		show: 5 // кол-во отображаемых блоков
	}

	$.fn.twViewer = function(options) {
		var carousel = {};
		var el = this;
		var elPos = 0;
		
	
		var init = function () {
			carousel.settings = $.extend({}, defaults, options);
			setup();
			

			var horizontal = function() {
				carousel.step = $(carousel.items).outerWidth(); // шаг карусели == ширине блока
				carousel.elWidth = carousel.settings.show * carousel.step; // вычисление ширины viewport
				carousel.elHeight = $(el).outerHeight(); // вычисление высоты viewport
				// максимальная координата для остановки обработки клика по кнопке контроля
				carousel.maxPosX = carousel.elWidth - carousel.items.length * carousel.step;
			
				// установка высоты viewport и ширины wrapper
				$(carousel.viewport).outerHeight(carousel.elHeight);
				$(carousel.wrapper).outerWidth(carousel.elWidth);
			}
			var vertical = function() {
				carousel.step = $(carousel.items).outerHeight(); // шаг карусели == высоте блока
				carousel.elHeight = carousel.settings.show * carousel.step; // вычисление высоты viewport
				carousel.elWidth = $(carousel.items).outerWidth(); // вычисление высоты viewport
				// максимальная координата для остановки обработки клика по кнопке контроля
				carousel.maxPosX = carousel.elHeight - carousel.items.length * carousel.step;
			
				// установка высоты viewport и ширины wrapper
				$(carousel.viewport).outerHeight(carousel.elHeight);
				$(carousel.wrapper).outerWidth(carousel.elWidth);
			}
			//console.log(carousel.settings.mode);
			if(carousel.settings.mode == 'horizontal') { 
				horizontal() 
			}
			else {
				vertical();
			}
		}

		var setup = function () {
			el.wrap('<div class="' + carousel.settings.wrapperClass + 
				'"><div class="' + carousel.settings.viewportClass + 
				'"></div></div>')
				.addClass('tw-items');

			var build = {
				controls:  function () {
				if(carousel.settings.mode == 'horizontal') {
					return '<div class="tw-controls"><div class="left"></div><div class="right"></div></div>';
				}
				else {
					return '<div class="tw-controls"><div class="top"></div><div class="bottom"></div></div>';
					}
				}

			}			

			carousel.viewport = el.parent();
			carousel.wrapper = carousel.viewport.parent();
			carousel.items = el.children();
			carousel.viewport.after(build.controls());
			carousel.controls = $(carousel.wrapper).find('.tw-controls').children();

			carousel.wrapper.addClass(carousel.settings.mode);		
		}
		el.move = {
			left: function(step, pos) {
				if(pos < 0) {
					$(el).stop().animate({ left: pos + step + "px" });
					pos += step;
				}
				return pos;
			},
			right: function(step, pos) {
				if(pos > carousel.maxPosX) {
					$(el).stop().animate({ left: pos - step + "px" });
					pos -= step;
				}
				return pos;
			},
			top: function(step, pos) {
				if(pos < 0) {
					$(el).stop().animate({ top: pos + step + "px" });
					pos += step;

				}
				return pos;
			},
			bottom: function(step, pos) {
				if(pos > carousel.maxPosX) {
					$(el).stop().animate({ top: pos - step + "px" });
					pos -= step;
				}
				return pos;
			}
		}
		

		init();
		// по классу определяется направление клика
		// move возвращает текущую позицию
		$(carousel.controls).on('click', function() {
			elPos = el.move[$(this).attr('class')](carousel.step, elPos);
		});
		return this;
	}
})(jQuery);





