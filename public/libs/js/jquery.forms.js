(function($) { 
	$.fn.clicker = function() {
		var blocks = this;
		$(blocks).each(function(index, el) {
			$(el)
				.focusin(function() {
					if(!$(this).val())
						$(this).prev('label').fadeOut();
				})
				.focusout(function() {
					if(!$(this).val())
						$(this).prev('label').fadeIn();
				});
		});
	}
})(jQuery);





